(function(){


angular
    .module("TaskApp")
    .service("Service", Service);

Service.$inject =['$http'];

 function Service($http) {
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the department data received from the calling function
            // to the /departments route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function\
            var ser = this;

            ser.insertTask = insertTask;
            ser.retrieveTask = retrieveTask;
            ser.deleteTask = deleteTask;
            ser.editTask = editTask;

            function insertTask(array){
                 console.log("in insertTask service: %s", array);
            
            return $http({  
                method: 'POST'
                , url: 'api/tasks'
                , data: {tasks: array}
                });
            };
 
            // retrieveDept retrieves department information from the server via HTTP GET.
           // Parameters: None. Returns: Promise object
        
            function retrieveTask(searchString) {
                console.log("in retrieveTask service");
            return $http({
                method: 'GET'
                , url: 'api/tasks'
                , params: {
                            'searchString': searchString
                        }
                });
            }   

            // deleteTask uses HTTP DELETE to delete tasks from database; passes information as route parameters.
            // IMPORTANT! Route parameters are not the same as query strings!
           
            function deleteTask(task_id) {
            return $http({
                method: 'DELETE'
                , url: 'api/tasks/' +task_id
            });

            }

            function editTask(onetask){
            return $http({
                method: 'PUT',
                url: "/api/tasks/" + onetask.task_id,
                   data: {
                    task_id:  onetask.task_id,
                    task_description : onetask.task_description, 
                    due_date : onetask.due_date,
                    category : onetask.category,
                    status  : onetask.status 
                }
            });
        }   

    }

})();