(function(){
    TaskApp = angular.module("TaskApp", []);

    TaskApp.controller("TaskCtrl", TaskCtrl);

    TaskCtrl.$inject =[ "Service", "$window" ];


    function TaskCtrl( Service, $window, ngMessages, ngAnimate){
        var TaskCtrl=this;
        TaskCtrl.task_id=0;
        TaskCtrl.task_description ="";
        TaskCtrl.due_date =""; //input
        TaskCtrl.formattedDate= ""; //output
        TaskCtrl.category="";
        TaskCtrl.newCategory="";
        TaskCtrl.status="Pending";
        TaskCtrl.array=[];
        TaskCtrl.categoryLabel=["Uncategorized", "Full Stack Foundation", "Personal Development", "Errands" ];
        TaskCtrl.filter="";
        TaskCtrl.array1=[];
        TaskCtrl.array3=[];
        TaskCtrl.stats = {
                            message: "",
                            code: ""        };
        TaskCtrl.object2={"task_id" : 3, "task_description":'Testing', "due_date":'01/01/2018', "category" : 'Errands', "status" : 'Pending'};
        
        
        
        // initializes with data from "tasks" schemas "tasks" table

        init();

            // The init function initializes view
        function init() {   
            console.log("Starting initialization");
            // We call Service.retrieveDeptDB to handle retrieval of department information. The data retrieved
            // from this function is used to populate search.html. Since we are initializing the view, we want to
            // display all available departments, thus we ask service to retrieve '' (i.e., match all)
            
             Service
                .retrieveTask('')
                .then(function (result) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    TaskCtrl.array = result.data;
                    // console.log(result.data);
                    console.log("result.data " + JSON.stringify(result.data));
                    console.log("Retreived 'tasks' Database Current Records")                  
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
            };    

            
        // adding a new task into the array of tasks
        TaskCtrl.add = function(){
            console.log("adding task_id: %s ; task_description: %s; due_date: %s ", TaskCtrl.task_id, TaskCtrl.task_description, TaskCtrl.due_date);
            TaskCtrl.formattedDate = TaskCtrl.due_date.toLocaleDateString('en-US');           
                console.log(TaskCtrl.formattedDate);
            TaskCtrl.array1= {
                "task_id": TaskCtrl.task_id,
                "task_description" :TaskCtrl.task_description,
                "category":TaskCtrl.category, 
                "due_date":TaskCtrl.formattedDate, 
                "status" : TaskCtrl.status  
                };
                console.log(TaskCtrl.array1);

            
            Service
                .insertTask(TaskCtrl.array1)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    console.log("inserted record Task ID : %s", TaskCtrl.array1.task_id);
                    init();
                    // $window.location.assign('/app/registration/thanks.html');
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    TaskCtrl.stats.message = err.data.name;
                    TaskCtrl.stats.code = err.data.parent.errno;
                });
                init();
            };
             




            // removing a task from the array of tasks : old code to delete from memory
            // TaskCtrl.delete = function(idx){
            // console.log(idx);
            // TaskCtrl.array.splice(idx,1);
            // }

             // Deletes record from tasks database.
            TaskCtrl.delete = function (idx) {
                var TaskID = TaskCtrl.array[idx].task_id; 
                console.log("Request to delete record task-id: %s", TaskID);

            Service
                .deleteTask(TaskID)
                .then(function (result) {
                    console.log("Deleted record with Task ID: %s", TaskID);
                    init();
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error: \n" + JSON.stringify(err));

                });
        }





            // using findIndex of task matching for the 1st item in array will have problem when two or more tasks or same     
            // TaskCtrl.delete=function(placeholder){  
            //     var idx=TaskCtrl.array.findIndex(function(elem){
            //         return(placeholder == elem.task)
            //     })
            //     if (idx>=0){
            //         TaskCtrl.array.splice(idx,1);
            //     console.log(idx);
            //     }
                
            //         //splice(3,1,) means go to the 4th basket and remove 1, starting from it
            // }

            //adding a new category
                TaskCtrl.addCategory=function(){
                    TaskCtrl.categoryLabel.push(TaskCtrl.newCategory);
                console.log(TaskCtrl.categoryLabel);
            }    

            //adding a check function to mark completed Task
                TaskCtrl.check =function(idx){
                    TaskCtrl.array[idx].status = "Done"
                }

            // edit changes


            TaskCtrl.newField = [];
                TaskCtrl.editing = false;
            TaskCtrl.editArray = function(idx) {
                    console.log(idx);  
                    
                    console.log(TaskCtrl.array[idx]);
                    TaskCtrl.newField = angular.copy(TaskCtrl.array[idx]);
                    
                    Service
                        .editTask(TaskCtrl.newField);
                        init();
                }


            // cancel changes in editing

   
            TaskCtrl.cancel = function(index) {

            TaskCtrl.array[index] = TaskCtrl.newField[index];
            TaskCtrl.editing = false;
        
            };

    }
})();





