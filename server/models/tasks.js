// Define tasks table model
// Model for tasks table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/

//Create a model for tasks table
module.exports = function(conn, Sequelize) {
    var tasks =  conn.define('tasks', {

        task_id: {
            type: Sequelize.INTEGER(5),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        task_description: {
            type: Sequelize.STRING,
            allowNull: false
        },
        due_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        category: {
            type: Sequelize.STRING,
            allowNull: false
        },
        status: {
            type: Sequelize.ENUM('Pending', 'Done'),
            allowNull: false
        }
    }, {
        tableName: 'tasks',
        timestamps: false
    });
    return tasks;
};