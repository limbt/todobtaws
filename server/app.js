var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");

const NODE_PORT = process.env.NODE_PORT || 3000; //SC: @TF & BT Please note port is 8888

const CLIENT_FOLDER = path.join(__dirname + '/../client'); 
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages'); //not in use yet as of 24 feb

const MYSQL_USERNAME = 'limbte';
const MYSQL_PASSWORD = 'b0224t'; //SC: @TF & BT Please remember to change password to match yours

const API_TASKS_ENDPOINT = "/api/tasks"; //cannot apply to static

var app = express();

//Creating a MySQL connection
var conn = new Sequelize(
    'tasks',            //SC: @BT Please note that the name of the new database to be created is tasks
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

//SC: @TF & BT Pls 1) create a new schema labelled as 'tasks' on mySQL and 2) complete tasks.js, before running the following
//create task table in database
//no associations as we only have one table in our database

conn.authenticate(function(){
    console.log("DB connected");
}).catch(function(err){
    console.log(err);
})

var tasks = require('./models/tasks')(conn, Sequelize);  //referencing to task.js

conn.sync({
    force:true
}).then( function(){
    console.log("Sync db !");
    tasks.create(
    {
        task_id: 1,
        task_description: "Task is loaded from MySQL",
        category: "Uncategorized",
        due_date: new Date(),
        status: "Pending"
    })
    tasks.create({
        task_id: 2,
        task_description: "Watch Youtube video on hair styling",
        category: "Personal Development",
        due_date: new Date(),
        status: "Pending"
    })
    tasks.create({
        task_id: 3,
        task_description: "Buy movie tickets",
        category: "Errands",
        due_date: new Date(),
        status: "Pending"
    })
    tasks.create({
        task_id: 4,
        task_description: "Set up an in-kitchen hydroponics farm",
        category: "Errands",
        due_date: new Date(),
        status: "Pending"
    })
    tasks.create({
        task_id: 5,
        task_description: "Buy tea for everyone",
        category: "Full Stack Foundation",
        due_date: new Date(),
        status: "Pending"
    })
    tasks.create({
        task_id: 6,
        task_description: "Complete group assignment",
        category: "Full Stack Foundation",
        due_date: new Date(),
        status: "Pending"
    }    
    );
});

//Middleware
app.use(express.static(CLIENT_FOLDER));     //prompts server to look for index.html in client folder
app.use(bodyParser.json());     //for POST to work
app.use(bodyParser.urlencoded ({extended: false}))  //disable acceptance of other forms

/*StackUp best practice
* GET /api/tasks - get all task entries
* POST /api/tasks - save a task entry
* DELETE /api/tasks/:task_description - delete a task entry based on task_description
* PUT /api/tasks/:task_description - update a task_description
*/

app.get(API_TASKS_ENDPOINT, function(req, res){
    console.log("GET Request query searchString : " + req.query.searchString);
    tasks 
        .findAll({
            where:  {
                $or: [   
                    { task_description: {$like: "%" + req.query.searchString + "%"} }, 
                    { category: {$like: "%" + req.query.searchString + "%"} }
                ]
            } 
        })
        .then(function(tasks){
            res.status(200).json(tasks);
        })
        .catch(function(err){
            res.status(500).json(err);
        })
})

app.post(API_TASKS_ENDPOINT, function(req,res){
    console.log("Inserting new task entry"); 
    console.log(req.body);
    tasks
        .create({
            task_id: req.body.tasks.task_id,
            task_description: req.body.tasks.task_description,
            due_date: req.body.tasks.due_date,
            category: req.body.tasks.category,
            status: req.body.tasks.status,
        }).then (function(tasks){      
            res.status(200).json(tasks);
        }).catch (function(err){
            res.status(500).json(err);
        })
});

/*Postman POST object sample
{"task": {
	"task_id":"7",
	"task_description": "Super postman",
    "due_date": "02/02/2017",
    "category":"Uncategorized",
    "status": "Done"
}}*/
 /*{"tasks": {
	"task_id":"10",
	"task_description": "Super postman",
    "due_date": "02/02/2017",
    "category":"Uncategorized",
    "status": 0
    }} */

////API PUT & DELETE are work in progress
app.put(API_TASKS_ENDPOINT + "/:task_id", function(req,res){ 
    var where = {};  //creating a placeholder called WHERE
    where.task_id = req.params.task_id;     //notice that this is NOT from the BODY. PARAMS refer to URL
    //line above sets the dept_no that YOU specify on URL, as a PROPERTY of WHERE
    tasks
        .update({task_description: req.body.task_description},
                {where: where} //the first where here is a sequelize key
        ).then (function(result){   //check using postman localhost:8080/api/departments/d012
            res.status(200)
                .json(result);
        }).catch(function(err){
            res.status(500)
                .json(err);
        })
});

// -- Searches for and deletes manager of a specific department.
// Client sent data as route parameters, we access route parameters (named routes) via the req.params property
app.delete(API_TASKS_ENDPOINT + "/:task_id", function (req, res) {
    var where = {};
    where.task_id = req.params.task_id;
    
    // The tasks table's primary key is task_id
    
    tasks
        .destroy({
            where: where
        })
        .then(function (result) {
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});
        })
        .catch(function (err) {
            console.log("-- DELETE /api/tasks/:task_id catch(): \n" + JSON.stringify(err));
        });
});



app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));      //SC: @BT 404.html not set up yet bc not covered in class yet
});

app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));   //SC: @BT 501.html not set up yet bc not covered in class yet
});

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});